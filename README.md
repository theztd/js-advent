![Build Status](https://gitlab.com/theztd/js-advent/badges/master/build.svg)

---

# Adventni kalendare

Jednoducha aplikace na tvorbu adventnich kalendaru. Zatim je nutne kazdy novy kalendar vytvorit rucne pomoci textoveho editoru. 


**Pristupne na adrese [https://advent-pro-tebe.kurzy-online.eu/sablona/](https://advent-pro-tebe.kurzy-online.eu/sablona/)**


![Example advent calendar](preview.jpg)

## Zalozeni noveho kalendare

Zkopirujte adresar sablona do **public/nejake-jmeno-kalendare** (nazev nesmi obsahovat mezery, proto pouzijte pomlcky). Cokoli ted zmenite v novem adresari **public/nejake-jmeno-kalendare** bude se tykat jen tohoto noveho kalendare.


## Zmena obrazku kalendare

Obrazek musi byt ve formatu jpg, nebo gif a nahrava se do adresare **public/nejake-jmeno-kalendare/images/**. Obrazek je potreba po nahrati prejmenovat na background.jpg, nebo background.gif. Pokud pouzijete soubor s koncovkou gif (vetsin ou animace), je jeste nutne v souboru **public/nejake-jmeno-kalendare/index.html** prepsat **background.jpg** na **background.gif**, jinak nebude obrazek funkcni.

## Zpravy textu pro jednotlive dny

Kazdy kalendar obsahuje soubor **public/nejake-jmeno-kalendare/scripts/messages.js**. Tento soubor obsahuje zpravy pro konkretni kalendar, zpravy jsou uvedene pod sebou postupne a co radek, to zprava pro cislo dne.
Tento soubor zaroven obsahuje text titulku daneho kalendare a uvitaci zpravu.
