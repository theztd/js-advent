// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "English Advent Calendar";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Doplň do vánočních vět přítomný prostý nebo průběhový čas a užij si advent s angličtinou :)";

// zprava na kazdy den, co radek to okenko.
const messages = [
    "We ____ (decorate) the Christmas tree today, and we will bake Christmas cookies tomorrow.",
    "Every year, my children ____ (go) ice-skating on the frozen lake.",
    "We always ____ (meet) some old friends at the Christmas markets.",
    "My sister ____ (put) chocolate icing on my cookies now. I am sad because I ____ (like) only lemon icing. (icing=poleva)",
    "Be quiet! Baby Jesus ___ (bring) our presents at the moment! I can hear him in the living room!",
    "A: What ____ (you, do) right now? B: I ____ (wrap) a present for my best friend!",
    "On the 25th of December, our family usually ____ (drink) hot chocolate together.",
    "Don’t put the Nativity scene on the table. You know that I always ____ (put) it under the Christmas tree. (Nativity scene = betlémek)",
    "The carp ____ (bake) in the oven and the potato salad ____ (sit) in the fridge right now.",
    "My mom always ____ (buy) all of our Christmas presents in the summer.",
    "We ____ (watch) TV at the moment because our favorite Christmas fairy tale is on.",
    "A: _____ (you, like) gingerbread cookies? B: Yes, especially with icing! (gingerbread = perník)",
    "When it snows outside, my friends usually ____ (build) a big snowman in the garden.",
    "I am bored because I ____ (clean) the house right now. We ____ (get) it ready for Christmas decorations.",
    "I know where my presents are because my parents always ____ (hide) the presents under their bed.",
    "In the U.S., small children often _____ (leave) cookies and milk for Santa next to the Christmas tree. It is their tradition.",
    "Santa ____ (ride) in his magical sleigh in the sky right now. Can you see him? (sleigh=sáně)", 
    "The children are very excited because they ____ (open) their presents at the moment.",
    "At Christmas, I usually ____ (invite) my friends to my house to watch fairy tales with me.",
    "My brother never ____ (fast) on the 24th of December. He ____ (like) to eat too much. (fast=postit se)",
    "My friends ____ (play) outside in the snow now. They are having a snowball fight behind the house.",
    "My grandmother always ____ (put) snowflake stickers on the windows to decorate for Christmas.",
    "_____ (you, usually open) your presents carefully or very quickly?",
    "In my family, we ____ (love) the festive atmosphere at Christmas. We always ____ (enjoy) getting together very much! (get together= setkat se)"
];
