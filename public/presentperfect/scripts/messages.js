// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "English Advent Calendar";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Doplň Past Simple/Present Perfect a užij si advent s anglickými větami :)";

// zprava na kazdy den, co radek to okenko.
const messages = [
   "It is snowing outside. It SNOW since 5 AM.",
    "We lit up the first candle of the Advent Wreath last night. We NOT LIGHT up any other candles yet.",
    "A: (you) BUY the Christmas present for your grandmother yet? B: Of course! I HIDE it under your bed yesterday.",
    "Have you got the house ready for Christmas decorations ALREADY? / YET?",
    "He BUY a fire extinguisher last week in case our Christmas tree caught on fire.",
    "I NOT FAST on the 24th of December, so I NOT SEE the golden pig.",
    "You should stop eating now. How many gingerbread cookies and vanilla crescents you EAT so far?",
    "We have been putting off decorating the tree for too long. = We NOT DECORATE the Christmas tree yet.",
    "A: Could you put the Christmas lights on the roof today, honey? B: But I already DO it!",
    "Three years ago, we DECIDE to celebrate Yule instead of Christmas. We NOT REPEAT that tradition since then.",
    "A: (you ever) RETURN an unwanted present? B: I DO it many times before. Actually, I RETURN one last year.",
    "We LEAVE an extra empty plate on the table on Christmas Eve in case a strange and hungry person appeared at our house.",
    "I HIDE his present a month ago, but I hid it too well. I LOOK FOR this present for hours, but I still NOT FIND it.",
    "Honey, have you cleaned your room SO FAR? / YET? I want to put up some decorations!",
    "When Baby Jesus RING his bell in the living room, all of the children RUN to the Christmas tree.",
    "My girlfriend PUT the carp in a pan with milk yesterday, but she NOT PEEL the potatoes for the potato salad yet.",
    "I always BELIEVE that Baby Jesus exists. He always brings me the most unexpected and wonderful presents.", 
    "I never SEE such a beautifully decorated Christmas tree before!",
    "Our family loves Christmas traditions, that's why we POUR lead yesterday.",
    "A: Who knows about this Norwegian Christmas tradition? B: I do! I KNOW about this special activity for years! Actually, I PRACTICE this tradition ever since I COME back from a college exchange in Norway.",
    "Jack FORGET the carp in the oven on Christmas Eve, and the kitchen BE full of smoke.",
    "Baby Jesus is running around our terrace. (?)How many footsteps he MAKE in the snow so far?",
    "A: (?) Why the star of Bethlehem APPEAR in the eastern sky according to the Bible? B: It APPEAR when Baby Jesus BE born.",
    "It is always lovely to meet our old friends at the Christmas markets. We MEET our former colleagues yesterday, and we are meeting my brother today."
];
