// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "English Advent Calendar";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Užij si advent s should, could, would :) Věty s hvězdičkou jsou těžší - s minulými tvary should nebo could.";

// zprava na kazdy den, co radek to okenko.
const messages = [
    "A: _____ you help me bake these Christmas cookies, please? I don’t have enough time. B: Sure. I’ll do it.",
    "Why ____ I put up Christmas decorations in October? It does not make any sense.",
    "(she) “I will buy more Christmas presents next week.” → She said that she ____ buy more presents the following week. (will changes to ??? in reported speech)",
    "My parents cannot come this year. They ___ come if they could.",
    "A: What ____ the devil do with a child who misbehaves? B: He ___ put the child in his sack and take the child to hell.",
    "You are a very good and nice boy. St. Nicholas ____ not bring you any gifts if you were not such a nice boy.",
    "You ____ always clean your house or apartment before Christmas holidays. It is the best time of the year to do it.",
    "_____ you mind sending me an electronic Christmas card? I don’t like the paper ones.",
    "On Christmas Eve, when everybody gathers around the dinner table, you ____ leave an extra plate on the table for a stranger.",
    "** She _____  gone to Italy for Christmas, but she decided to stay in Prague instead.",
    "Baby Jesus ____ always come after Christmas dinner, but not sooner. That is the tradition.",
    "A: We ____ really decorate the Christmas tree today. B: ____ we do it tomorrow, please? I don’t feel like doing it today.",
    "We have a couple options for our Christmas dinner menu. We ____ grill some carp, or we ____ fry some schnitzels.",
    "**My father ____ put up the Christmas decorations last week, but he got lazy and forgot about it. My mother asked him to do it many times last week, and now she is angry at him. ",
    "When you get an advent calendar filled with chocolate candy, you ____ only eat one piece of chocolate per day.",
    "Why ____ you want to get a giant baby doll for Christmas? (=I don’t understand why someone wants it.)",
    "Our children ____ like to go ice skating on the frozen lake today. They have been talking about it for a week now.", 
    "(my parents) “We will not give you any presents this year.” → My parents said that they ____ not give me any presents. (will changes to ??? in reported speech).",
    "**It’s too bad you didn’t come last weekend. You ____ come to visit us instead of spending Christmas alone!",
    "We ____ like to receive presents from Baby Jesus as well as Santa Claus, so that we could have more presents in total.",
    "** I _____ bought all of the presents in November, but I waited to do it until the 23rd of December instead.",
    "A: What ____ we do on the 24th of December? B: We ____ fast during the day and eat only dinner on Christmas Eve.",
    "I hope that Baby Jesus comes tomorrow. If he did not come, many children ____ be very sad.",
    "Merry Christmas!____ you send me a Merry Christmas message, please ? :)"
];
