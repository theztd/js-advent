// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "English Advent Calendar";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Doplň Past Simple/Present Perfect nebo kondicionály do vánočních vět a užij si advent s angličtinou :)";

// zprava na kazdy den, co radek to okenko.
const messages = [
    "It is very muddy outside, and there is no chance of snow this week. If it SNOW, I TAKE my children sledding on the hill.",
    "We lit up the first candle of the Advent Wreath last night. If we NOT LIGHT it up, we MISS the first Sunday of Advent.",
    "A: (you) BUY the Christmas present for your grandmother yet? B: Of course! I HIDE it under your bed yesterday.",
    "Have you got the house ready for Christmas decorations ALREADY? / YET?",
    "Please, buy a fire extinguisher IN CASE? / IF? our Christmas tree catches on fire.",
    "If you FAST on the 24th of December, you SEE the golden pig!",
    "You should stop eating now. How many gingerbread cookies and vanilla crescents you EAT so far?",
    "We have been putting off decorating the tree for too long. If we START decorating the Christmas tree a week ago, we NOT HAVE TO decorate it under such stress now.  (pozor, jedná se o mixed conditional!)",
    "A: Could you put the Christmas lights on the roof today, honey? B: But I already DO it!",
    "Three years ago, we DECIDE to celebrate Yule instead of Christmas. We NOT REPEAT that tradition since then.",
    "A: (you ever) RETURN an unwanted present? B: I DO it many times before. Actually, I RETURN one last year.",
    "We always leave an extra empty plate on the table on Christmas Eve IN CASE? / IF? a strange and hungry person appears at our house.",
    "I hid his present a month ago, but I hid it too well. I LOOK FOR this present for hours, but I still NOT FIND it.",
    "Honey, have you cleaned your room SO FAR? / YET? I want to put up some decorations!",
    "When Baby Jesus RING his bell in the living room, all of the children RUN to the Christmas tree.",
    "My girlfriend PUT the carp in a pan with milk yesterday, but she NOT PEEL the potatoes for the potato salad yet.",
    "I always BELIEVE that Baby Jesus exists. He always brings me the most unexpected and wonderful presents.", 
    "I never SEE such a beautifully decorated Christmas tree before!",
    "Our family loves Christmas traditions. If we NOT ENJOY the traditions so much, we NOT POUR lead in the kitchen every year.",
    "A: Who knows about this Norwegian Christmas tradition? B: I do! I KNOW about this special activity for years! Actually, I PRACTICE this tradition ever since I COME back from a college exchange in Norway.",
    "Jack forgot the carp in the oven. If he NOT FORGET the carp, we NOT HAVE to call the firefighters on New Years Eve.",
    "There are some strange footsteps in the snow. If I BELIEVE in the existence of Baby Jesus, I SAY that he ran across our terrace.",
    "A: (?) Why the star of Bethlehem APPEAR in the eastern sky according to the Bible? B: It APPEAR when Baby Jesus BE born.",
    "It is always lovely to meet our old friends at the Christmas markets. If we CANNOT meet our friends, Christmas BE a much less festive season."
];
