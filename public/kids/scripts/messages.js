// ve kterem mesici se advent odpocitava (default je 12 pro prosinec)
const monthInt = 12;

// Image path (nefunguje)
// const background_image = "images/background.jpg";

// nadpis na strance (idealne do 50i znaku kuli formatovani)
const title = "English Advent Calendar";

// uvitaci text na titolni strance, delka muze byt libovolna
const welcomeMsg = "Užij si advent s anglickými úkoly a hraním :)";

// zprava na kazdy den, co radek to okenko.
const messages = [
    "Kolik svíček dáváme na ADVENT WREATH = adventní věnec? Který den se svíčky zapalují? (odpověď napiš nebo řekni v angličtině)",
    "Určitě píšeš dopis Ježíškovi. Dokážeš popsat dárky, které si přeješ, i v angličtině? U každého dárku rozhodni, jestli je BIG = velký nebo SMALL = malý. Který dárek je pro tebe THE BEST PRESENT = nejlepší dárek?",
    "Jaká je tvoje nejoblíbenější CHRISTMAS COOKIE = cukroví? Máš rád/a GINGERBREAD COOKIES = perníčky? Nakresli 3 různé tvary GINGERBREAD COOKIES, které o Vánocích pečete. Pokud to zvládneš, pojmenuj tvary v angličtině. Třeba já peču tvar sněhuláka = SNOWMAN.",
    "Nakresli Vánoční stromeček a namaluj na něj baňky. Ale pozor :) - na stromečku musí být tolik baněk, aby se do nich vlezl celý název Vánoc v angličtině. V každé baňce bude jenom jedno písmenko. (Vánoce = C H R I S T M A S ) Kolik máme baněk? (odpověď napiš nebo řekni v angličtině)",
    "Jak se řekne anglicky pohádka? Máš je rád/a? Napiš anglicky: Mám rád/a pohádky. Nebo Nemám rád/a pohádky. Moje oblíbená (FAVORITE) pohádka je.....",
    "Na CHRISTMAS EVE = Štědrý večer jíme speciální jídla. Rozluštíš z angličtiny, o jaké jídlo se jedná? FISH (CARP), POTATO SALAD, FISH SOUP, CHRISTMAS COOKIES, APPLES….napadají tě další jídla, která doma jíte na Štědrý večer?",
    "Nakresli speciální ADVENT WREATH, kde každá svíčka bude představovat jiné roční období. Napiš ke každé svíčce název jednoho ročního období v angličtině. SPRING / SUMMER / AUTUMN / WINTER. Zakroužkuj, kterou svíčku/které období máš nejradši.",
    "Napiš nebo nakresli jeden dárek, co si přeješ pod stromeček, a vyjmenuj všechny barvy v angličtině, které bude dárek mít. (Třeba panenka - DOLL - pink dress, orange head, black eyes, a tak dále)",
    "Někdo si dává pod CHRISTMAS TREE betlémek. V angličtině mu říkáme NATIVITY SCENE. (Je to obrázek z chléva, kde se narodil Ježíšek.) Dokážeš přeložit, jaká zvířátka se v NATIVITY SCENE nachází?  DONKEY, COW, SHEEP, CAMEL, CHICKEN...napadají tě i další zvířátka?",
    "O vánocích se schází celá rodina. Napiš nebo vyjmenuj v angličtině, kteří členové rodiny se u vás sejdou. Třeba Pavla - MOTHER, Pepa - UNCLE,... a tak dále.",
    "Napiš nebo řekni co nejvíc sportů, které děláme o Vánocích, když nasněží nebo když zamrzne voda na rybníku. Když to zvládneš, zkus je říct nebo napsat v angličtině.",
    "CHRISTMAS = Vánoce začínají v angličtině na písmenko C (protože v angličtině nemáme písmenko CH). Kolik dalších slov, které začínají na c, vymyslíš v angličtině? Třeba CAT a tak dále...",
    "O Vánocích si budeme přát MERRY CHRISTMAS = Veselé Vánoce. Já každý rok posílám malá přání k Vánocům, která si vyrábím. Pokud máš chuť, vyrob někomu, koho máš rád/a takové přání v angličtině :)",
    "Nakresli, jak si představuješ, že vypadá BABY JESUS = Ježíšek. Až to budeš mít hotové, napiš k obrázku i jeho jméno v angličtině.",
    "CHRISTMAS = Vánoce jsou svátek a nemusíme do školy ani do práce. V angličtině říkáme svátkům HOLIDAYS. What is your favorite holiday? = Jaký je tvůj nejoblíbenější svátek? Může to být třeba EASTER = Velikonoce, nebo CHRISTMAS = Vánoce, nebo SUMMER HOLIDAYS = letní prázdniny….napadají tě další svátky, které máš rád/a?",
    "Zvířátka slaví Vánoce. U stolu na CHRISTMAS EVE = Štědrý večer se sešly i zvířátka, ale každé má jinou náladu. Nakresli zvířátka u stolu a nakresli, jak se tváří. DOG - HAPPY, CAT - SLEEPY, HORSE - ANGRY, SHEEP - HUNGRY, GOAT - CRYING, FISH - SCARED, RABBIT - EXCITED",
    "Na papír si nakresli 1 CHRISTMAS ORNAMENT = baňku a do ní napiš datum, kdy slavíme Štědrý večer. Dokážeš čísla přečíst anglicky? Jak se jmenuje anglicky ten měsíc?", 
    "Co je právě za měsíc? Který měsíc byl před tímto měsícem, a který měsíc bude po něm? (odpověď napiš nebo řekni i anglicky)",
    "Vánoce slavíme v zimě. Co můžeš vidět za oknem a v přírodě, když je pořádná zima? Jaká slovíčka tě napadnou, když mluvíme o zimě? Nápověda je v angličtině :) WINTER - SNOW, ICE, SNOWFLAKES, SNOWMAN, COLD, ….",
    "A: Co to je NEW YEAR’S EVE? (NEW = nový, YEAR = rok, EVE = večer) Co většinou v tento den děláme?",
    "Který den v týdnu bude letos Štědrý večer? (odpověď napiš nebo řekni anglicky)",
    "Nakresli domeček, kde jsou šílené Vánoce, podle následujících instrukcí. Když tě napadnou další šílené věci, klidně je dokresli, a pojmenuj je v angličtině. 1. CARP ON THE BED, 2. CHRISTMAS TREE UPSIDE DOWN = vzhůru nohama 3. PRESENTS ON THE ROOF = střecha, 4. POTATO SALAD ON THE TV, 5. SNOW IN THE HOUSE, 6. CHRISTMAS ORNAMENTS= ozdoby ON THE TABLE IN THE KITCHEN",
    "Když budeš zítra držet půst, co uvidíš? V angličtině tomu říkáme GOLDEN PIG :)",
    "Zvládl/a jsi všechny Vánoční úkoly! Super dobrá práce! :) Dnes popřej všem MERRY CHRISTMAS! = Veselé Vánoce!"
];
